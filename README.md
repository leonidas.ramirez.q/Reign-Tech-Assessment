# Reign Tech Assessment

This API fetches node-related posts from the Hacker News API. It uses a cron job for fetching new node.js posts every hour

## Scripts

### Install dependencies

```bash
  npm run install
```

### Start the server

```bash
  npm run start
```

### Run tests

```bash
  npm run test
```

## Considerations Before Running the Project:

- Swagger Documentation can be found at:
```bash
  /swagger.yml
```
- Its important to create simple database at the  [MongoDB Atlas Website](https://www.mongodb.com/atlas/database)

- Once the database is created you must create a new .env file with the following parameters:
```bash
  PORT=8000
  MONGO_URL="<paste the mongo URL that the MongoDB Atlas Platform gave you>"
```
- The cronjob that was created for fetching the node.js posts from Hacker News will be executed every hour at the 59th minute.

- Once the project is running in your local machine you can consume this API through its Postman Collection:
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/14464237-95952559-e1ba-4dab-99b4-1b1c75c53de2?action=collection%2Ffork&collection-url=entityId%3D14464237-95952559-e1ba-4dab-99b4-1b1c75c53de2%26entityType%3Dcollection%26workspaceId%3De547d577-7b42-4930-8d33-333b679e2b61)
