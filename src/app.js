const express = require("express");
const cors = require("cors");
const morgan = require("morgan");

const api = require("./routes/api");

const app = express();

app.use(
  cors({
    origin: "http://localhost:8000",
  })
);
app.use(morgan("combined"));

app.use(express.json());

app.use("/v1", api);

app.get("/", (req, res) => {
  res.send("This is my Hacker News API");
});

module.exports = app;
