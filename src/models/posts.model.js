const axios = require("axios");

const postsDB = require("./posts.mongo");

const HACKER_NEWS_API_URL =
  "https://hn.algolia.com/api/v1/search_by_date?query=nodejs";

async function populatePosts() {
  try {
    console.log("Downloading Posts Data...");

    const response = await axios.get(HACKER_NEWS_API_URL);

    const postHits = response.data.hits;

    if (response.status !== 200) {
      console.log("Problem downloading posts data");
      throw new Error("Post data download failed");
    }

    for (const postHit of postHits) {
      const post = {
        objectId: postHit["objectID"],
        createdAt: postHit["created_at"],
        title: postHit["title"],
        url: postHit["url"],
        author: postHit["author"],
      };

      const postExists = await exitsPostWithId(post.objectId);

      if (!postExists) {
        console.log(
          `New post retreived! creation date: ${post.createdAt} object id: ${post.objectId}`
        );
        await savePost(post);
      }
    }
  } catch (error) {
    console.error(error);
  }
}

async function findPost(filter) {
  return await postsDB.findOne(filter);
}

async function exitsPostWithId(objectId) {
  return await findPost({
    objectId: objectId,
  });
}

async function getAllPosts(skip, limit) {
  return await postsDB
    .find({}, { _id: 0, __v: 0 })
    .sort({ createdAt: 1 })
    .skip(skip)
    .limit(limit);
}

async function savePost(post) {
  await postsDB.findOneAndUpdate(
    {
      objectId: post.objectId,
    },
    post,
    {
      upsert: true,
    }
  );
}

async function deletePost(id) {
  await postsDB.deleteOne({
    objectId: id,
  });
  return id;
}
module.exports = { populatePosts, getAllPosts, deletePost, exitsPostWithId };
