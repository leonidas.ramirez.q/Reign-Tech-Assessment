const mongoose = require("mongoose");

const postsSchema = new mongoose.Schema({
  objectId: {
    type: Number,
    required: true,
  },
  createdAt: {
    type: Date,
    required: true,
  },
  title: {
    type: String,
    required: false,
  },
  url: {
    type: String,
    required: false,
  },
  author: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("Post", postsSchema);
