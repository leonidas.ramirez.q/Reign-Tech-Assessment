const {
  getAllPosts,
  exitsPostWithId,
  deletePost,
} = require("../../models/posts.model");

const { getPagination } = require("../../services/query");

async function httpGetAllPosts(req, res) {
  const { skip, limit } = getPagination(req.query);
  const posts = await getAllPosts(skip, limit);
  return res.status(200).json(posts);
}

async function httpDeletePost(req, res) {
  const objectId = Number(req.params.id);

  const existPost = await exitsPostWithId(objectId);
  if (!existPost) {
    return res.status(400).json({
      error: "Post not found",
    });
  }

  const deleted = await deletePost(objectId);
  if (!deleted) {
    return res.status(400).json({
      error: "Post was not deleted",
    });
  }
  return res.status(200).json(deleted);
}

module.exports = { httpGetAllPosts, httpDeletePost };
