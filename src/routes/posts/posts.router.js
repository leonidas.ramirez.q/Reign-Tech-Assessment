const express = require("express");
const { httpGetAllPosts, httpDeletePost } = require("./posts.controller");

const postsRouter = express();
/**
 * @openapi
 * /posts:
 *  get:
 *    description: This endpoint gets the posts stored in our databased sorted by creation date
 *    responses:
 *     200:
 *    description: All the posts were found
 */
postsRouter.get("/", httpGetAllPosts);

postsRouter.delete("/:id", httpDeletePost);

module.exports = postsRouter;
