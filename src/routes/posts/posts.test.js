const request = require("supertest");
const app = require("../../app");
const { mongoConnect, mongoDisconnect } = require("../../services/mongo");

describe("Posts API", () => {
  beforeAll(async () => {
    await mongoConnect();
  });

  afterAll(async () => {
    await mongoDisconnect();
  });

  describe("Test for GET /posts", () => {
    test("It should respond with 200 status", async () => {
      await request(app)
        .get("/v1/posts")
        .expect("Content-Type", /json/)
        .expect(200);
    });
  });
});
