const http = require("http");

require("dotenv").config();

const app = require("./app");
const { mongoConnect } = require("./services/mongo");
const { populatePosts } = require("./models/posts.model");

const PORT = process.env.PORT || 8000;

const server = http.createServer(app);

const nodeCron = require("node-cron");

async function startServer() {
  await mongoConnect();
  nodeCron.schedule("*/59 * * * * ", await populatePosts);

  app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}...`);
  });
}

startServer();
